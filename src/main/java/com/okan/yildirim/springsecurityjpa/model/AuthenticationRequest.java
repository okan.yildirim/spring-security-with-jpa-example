package com.okan.yildirim.springsecurityjpa.model;

import javax.validation.constraints.NotEmpty;

public class AuthenticationRequest {

    @NotEmpty(message = "authentication.request.username.is.empty")
    private String username;
    @NotEmpty(message = "authentication.request.password.is.empty")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
