package com.okan.yildirim.springsecurityjpa.entity;

public enum UserRole {
    ADMIN, USER
}
