package com.okan.yildirim.springsecurityjpa.config;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {
    private static final String TOKEN_TYPE = "Bearer ";

    private final JwtTokenProvider jwtTokenProvider;

    public JwtRequestFilter(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws IOException, ServletException {
        String jwtToken = getJwtFromRequest(httpServletRequest);

        if(StringUtils.hasText(jwtToken)) {
            String usernameFromJWT = jwtTokenProvider.getUsernameFromJWT(jwtToken);

            boolean valid = Objects.nonNull(usernameFromJWT) && jwtTokenProvider.validateToken(jwtToken)
                    && Objects.isNull(SecurityContextHolder.getContext().getAuthentication());

            if (valid) {
                UsernamePasswordAuthenticationToken authentication = jwtTokenProvider.getAuthentication(jwtToken);
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private String getJwtFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith(TOKEN_TYPE)) {
            return bearerToken.substring(7);
        }
        return null;
    }
}
