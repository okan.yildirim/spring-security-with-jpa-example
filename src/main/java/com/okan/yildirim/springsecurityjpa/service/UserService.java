package com.okan.yildirim.springsecurityjpa.service;

import com.okan.yildirim.springsecurityjpa.entity.User;
import com.okan.yildirim.springsecurityjpa.repository.UserRepository;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void createUser(User user) {
        userRepository.save(user);
    }

    public User getUser(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new UsernameNotFoundException("user is not found with id: " + id));
    }

    public User getUserByUserName(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("user is not found with username: " + username));
    }

    public List<User> getUsers() {
        return userRepository.findAll();
    }
}
