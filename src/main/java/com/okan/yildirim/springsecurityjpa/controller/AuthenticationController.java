package com.okan.yildirim.springsecurityjpa.controller;

import com.okan.yildirim.springsecurityjpa.config.CustomUserDetailsService;
import com.okan.yildirim.springsecurityjpa.config.JwtTokenProvider;
import com.okan.yildirim.springsecurityjpa.model.AuthenticationRequest;
import com.okan.yildirim.springsecurityjpa.model.AuthenticationResponse;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    private final AuthenticationManager authenticationManager;

    private final CustomUserDetailsService userDetailsService;

    private final JwtTokenProvider jwtTokenProvider;

    public AuthenticationController(@Qualifier("authenticationManagerBean")
                                            AuthenticationManager authenticationManager,
                                    CustomUserDetailsService userDetailsService,
                                    JwtTokenProvider jwtTokenProvider) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.jwtTokenProvider = jwtTokenProvider;
    }


    @PostMapping
    public AuthenticationResponse auth(@RequestBody @Valid AuthenticationRequest authenticationRequest) {

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword()));
        } catch (BadCredentialsException e) {
            throw new BadCredentialsException("Incorrect Email or password!");
        }

        UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        String jwtToken = jwtTokenProvider.generateToken(userDetails);

        return new AuthenticationResponse(jwtToken);
    }
}
