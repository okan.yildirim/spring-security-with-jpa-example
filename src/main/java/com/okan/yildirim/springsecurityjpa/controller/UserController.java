package com.okan.yildirim.springsecurityjpa.controller;

import com.okan.yildirim.springsecurityjpa.entity.User;
import com.okan.yildirim.springsecurityjpa.entity.UserRole;
import com.okan.yildirim.springsecurityjpa.service.UserService;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserController(UserService userService,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @GetMapping
    public String getHomeMessage() {
        return "Hello User";
    }

    @PostConstruct
    public void createUser() {
        String password = bCryptPasswordEncoder.encode("12345");
        userService.createUser(new User("okan", password, UserRole.USER));
        userService.createUser(new User("volkan", password, UserRole.USER));
        userService.createUser(new User("admin", password, UserRole.ADMIN));

    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        User user = userService.getUserByUserName(username);

        if (!user.getId().equals(id)) {
            throw new BadCredentialsException("Bad Credential Exception");
        }
        return userService.getUser(id);
    }

}
