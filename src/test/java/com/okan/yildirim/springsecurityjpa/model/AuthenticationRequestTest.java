package com.okan.yildirim.springsecurityjpa.model;

import org.junit.jupiter.api.Test;

import javax.validation.Validation;
import javax.validation.Validator;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

class AuthenticationRequestTest {

    private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Test
    void it_should_validate() {
        // given
        AuthenticationRequest request = new AuthenticationRequest();
        // when
        var validationResult = validator.validate(request).stream()
                .collect(Collectors.toMap(field -> field.getPropertyPath().toString(), error -> error));
        //then
        assertThat(validationResult).hasSize(2);
        assertThat(validationResult.get("username").getMessage()).isEqualTo("authentication.request.username.is.empty");
        assertThat(validationResult.get("password").getMessage()).isEqualTo("authentication.request.password.is.empty");
    }

}