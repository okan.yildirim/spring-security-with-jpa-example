package com.okan.yildirim.springsecurityjpa.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.okan.yildirim.springsecurityjpa.config.CustomUserDetailsService;
import com.okan.yildirim.springsecurityjpa.config.JwtAuthenticationEntryPoint;
import com.okan.yildirim.springsecurityjpa.config.JwtTokenProvider;
import com.okan.yildirim.springsecurityjpa.model.AuthenticationRequest;
import com.okan.yildirim.springsecurityjpa.model.AuthenticationResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = AuthenticationController.class)
class AuthenticationControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AuthenticationManager authenticationManager;

    @MockBean
    CustomUserDetailsService userDetailsService;

    @MockBean
    JwtTokenProvider jwtTokenProvider;

    @MockBean
    JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Test
    public void it_should_get_token() throws Exception {
        //given

        AuthenticationRequest authenticationRequest = new AuthenticationRequest();
        authenticationRequest.setPassword("password");
        authenticationRequest.setUsername("username");

        ObjectMapper objectMapper = new ObjectMapper();
        String request = objectMapper.writeValueAsString(authenticationRequest);

        given(authenticationManager.authenticate(any()))
                .willReturn(new UsernamePasswordAuthenticationToken("principal", "user"));

        User userDetails = new User(authenticationRequest.getUsername(), authenticationRequest.getPassword(), new ArrayList<>());
        given(userDetailsService.loadUserByUsername(authenticationRequest.getUsername()))
                .willReturn(userDetails);

        given(jwtTokenProvider.generateToken(userDetails)).willReturn("token");
        //when

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/auth")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)).andExpect(status().isOk());
        //then
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        AuthenticationResponse authenticationResponse = new AuthenticationResponse("token");
        assertThat(response.getContentAsString()).isEqualTo(objectMapper.writeValueAsString(authenticationResponse));
    }
}